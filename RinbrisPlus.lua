local LSM = LibStub('LibSharedMedia-3.0')
local statusBar = LSM.MediaType.STATUSBAR
LSM:Register(statusBar, 'RinbrisPlusOnePixel', [[Interface\Addons\RinbrisPlus\Media\statusbar\RinbrisPlusOnePixel]])

local setScale = CreateFrame('FRAME')
setScale:RegisterEvent('PLAYER_ENTERING_WORLD')
setScale:SetScript('OnEvent', function()
	UIParent:SetScale(0.7111111111)
	setScale:UnregisterAllEvents()
end)