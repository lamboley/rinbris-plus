## Interface: 90200
## Title: Rinbris Plus
## Author: Rinbris
## Version: 0.0.1

TaintLess.xml

#@no-lib-strip@
Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.xml
Libs\LibSharedMedia-3.0\lib.xml
#@end-no-lib-strip@

RinbrisPlus.lua

Modules\Miscellaneous\SellingJunks.lua
Modules\Miscellaneous\RepairAllItems.lua
Modules\Miscellaneous\CVars.lua
Modules\Miscellaneous\OrderHall.lua

Modules\ChatFrame.lua
Modules\GameTooltip.lua
Modules\ObjectiveTrackerFrame.lua